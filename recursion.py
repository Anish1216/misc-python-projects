"""
Name: recursion.py
Author: Anish Krishnan
Description: This program contains a set of simple recursive functions that operate on lists.
I originally wrote these in Scala, but converted them to Python to make use of the language's
ability to unpack values.

One limitation is that Python needs at least one value to unpack. This necessitates an extra
check in case the list is one element long.

"""

import sys

def help():
	print("\n-------- Info --------")
	print("\nRun the program followed by multiple command line arguments.")
	print("The first argument should be the function you wish to use, and the rest should be members of a list.")
	print("Example: python recursion.py unique 1 2 3 4")
	print()
	print("show: Prints the list")
	print("allEqual: Returns True if and only if all of the elements in the list are equal")
	print("findMax: Finds the maximum element in a list")
	print("rev: Reverses a list")
	print("unique: Returns True if the list contains all unique values, False if not")
	print("removeDupes: Removes adjacent duplicates from a list")
	print("contains: Returns True if a specified item is in a list, False if not")
	print("mapFunc: Maps an expression to a list")
	print("filt: Filters a list based on an expression")


def allEqual(list):
	x, *xs = list
	if not xs:
		return True
	else:
		if x != xs[0]:
			return False
		else:
			return allEqual(xs)


def findMax(list):
	x, *xs = list
	if not xs:
		return x
	else:
		return max(x, findMax(xs))


def rev(list):
	x, *xs = list
	if not xs:
		return list
	else:
		return rev(xs) + [x]	# Note that append() does not return a list, and list(x) does not work.


def unique(list):
	x, *xs = list
	if not xs:
		return True
	else:
		if x in xs:
			return False
		else:
			return unique(xs)


def removeDupes(list):
	x, *xs = list
	if not xs:
		return list
	else:
		if x == xs[0]:
			return removeDupes(xs)
		else:
			return [x] + removeDupes(xs)


def contains(list):
	def aux(list, num):
		x, *xs = list
		if not xs:
			if (x != num):
				return False
		if (x == num):
			return True
		else:
			return aux(xs, num)
	
	num = input("Enter a number: ")
	return aux(list, num)


def mapFunc(list):
	def aux(list, f):
		x, *xs = list
		x = int(x)	# Convert x to an integer so that "+" and "-" aren't interpreted as string operations.
		if not xs:
			return [f(x)]
		else:
			return [f(x)] + aux(xs, f)
	
	### Use eval() to parse the expression; then, pass in the new function into aux(). ###
	f = eval("lambda x: " + input("Enter an expression for x (e.g., x + 1): "))
	return aux(list, f)


def filt(list):
	def aux(list, f):
		x, *xs = list
		x = int(x)
		if not xs:
			if (f(x)):
				return [x]
			else:
				return []
		else:
			if (f(x)):
				return [x] + aux(xs, f)
			else:
				return aux(xs, f)

	f = eval("lambda x: " + input("Enter a test for x (e.g., x > 0): "))
	return aux(list, f)


def show(list):
	print([l for l in list])

	
def main(args):

	if len(args) < 2:
		help()
	
	else:
		function, *param = args
	
		print(
			eval(function)(param)	# Use eval() to parse the function.
		)

if __name__ == "__main__":
	main(sys.argv[1:])