# File: server.py
# Author: Anish Krishnan

import socket, threading, traceback

LOCALHOST = '127.0.0.1'
MAX_BYTES = 1024


class Worker(threading.Thread):
	def __init__(self, conn, addr):
		super(Worker, self).__init__()
		self.conn = conn
		self.addr = addr
		self.worker_loop = True

	# Invoked by worker.start()
	def run(self):
		super(Worker, self).run()
		print('Started worker thread.')

		# Receive, then decode data from client.
		while self.worker_loop:
			try:
				self.data_from = self.conn.recv(MAX_BYTES)
				if not self.data_from:
					self.worker_loop = False

				self.data_text = decode(self.data_from)
				print('Received:', ' + '.join(self.data_text.split()))
				self.send_back(self.data_text)

			except:
				print('ERROR: Unable to receive data')
				self.worker_loop = False
				traceback.print_exc()

	# Return data back to client.
	def send_back(self, data):
		print('Sending sum...', end = ' ')
		try:
			self.sum = self.evaluate(self.data_text)
			self.data_to = encode(self.sum)
			if not self.data_to:
				raise OSError('ERROR: No data to send')
				traceback.print_exc()

			self.conn.send(self.data_to)
		
		except:
			print('ERROR: Failed to send response')
			traceback.print_exc()

		finally:
			print('done.\n')

	# Must be converted to str type
	def evaluate(self, data):
		nums = [int(num) for num in data.split()]
		return str(sum([num for num in nums]))


def encode(text):
	return text.encode('UTF-8')

def decode(text):
	return text.decode('UTF-8')


def main():
	host = LOCALHOST
	port = 4848
	conn_info = (host, port)

	sock = socket.socket()
	sock.bind(conn_info)
	sock.listen(1)	# Listen for up to one connection.

	print()
	print('------------------------------------------')
	print('---- Anish\'s localhost Python server ----')
	print('------------------------------------------')

	print('Listening for a connection on {0} at port {1}...'.format(host, port))

	server_loop = True
	while server_loop:
		conn, addr = sock.accept()
		print('Connection successful.', end = ' ')
		worker = Worker(conn, addr)
		worker.start()
	conn.close()


if __name__ == '__main__':
	main()