# File: client.py
# Author: Anish Krishnan

import socket, traceback

LOCALHOST = '127.0.0.1'
MAX_BYTES = 1024

def encode(text):
	return text.encode('UTF-8')

def decode(text):
	return text.decode('UTF-8')

def main():
	host = LOCALHOST
	port = 4848
	conn_info = (host, port)

	print()
	print('------------------------------------------')
	print('---- Anish\'s localhost Python client ----')
	print('------------------------------------------')

	sock = socket.socket()
	sock.connect(conn_info)

	message = str()
	while message.strip() != ':q':
		message = input('Enter two or more numbers to send to the server (separated by spaces): ').strip()
		message_to = encode(message)

		if not message_to:
			raise OSError('ERROR: Empty message')
			traceback.print_exc()

		sock.send(message_to)

		data = decode(sock.recv(MAX_BYTES))
		print('Sum received from server:', str(data))
		print()
	sock.close()

if __name__ == '__main__':
	main()