# Multithreaded client/server program #
### Author: Anish K. ###
### Version: 1.0 ###

## Notes ##
- This program involves a client and multithreaded server connecting through the localhost (127.0.0.1) at port 4848
- Open the server first, then the client
- One can open multiple clients if one so wishes
- Enter a series of numbers into the client, separated by spaces
- The server should send the sum of those numbers